#Configuration file for cntk
BUILDTYPE=release
MATHLIB=mkl
MKL_PATH=/users/jagpreet/cntk/CNTKCustomMKL
MKL_THREADING=parallel
CNTK_CUSTOM_MKL_VERSION=3
CUDA_PATH=/opt/nvidia/cudatoolkit8.0/default/
GDK_INCLUDE_PATH=/opt/nvidia/cudatoolkit8.0/default/include
GDK_NVML_LIB_PATH=/opt/cray/nvidia/default/lib64/
CUB_PATH=/users/jagpreet/cntk/cub-1.4.1
CUDNN_PATH=/users/jagpreet/cntk/cudnn-5.1
OPENCV_PATH=/users/jagpreet/cntk/opencv-3.1.0
LIBZIP_PATH=/users/jagpreet/cntk/lib
CNTK_ENABLE_1BitSGD=true
BOOST_PATH=/users/jagpreet/cntk/boost-1.60.0
PROTOBUF_PATH=/users/jagpreet/cntk/protobuf-3.1.0
MPI_PATH=/opt/cray/pe/mpt/7.5.0/gni/mpich-gnu/5.1
CNTK_ENABLE_ASGD=true
