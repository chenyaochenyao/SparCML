import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
import numpy as np
import sys, re, os
from collections import OrderedDict
from math import sqrt 

#matplotlib.rcParams.update({'font.size':22})
fs=14
matplotlib.rcParams['axes.labelsize'] = fs;
matplotlib.rcParams['xtick.labelsize'] = fs;
matplotlib.rcParams['ytick.labelsize'] = fs;
matplotlib.rcParams['legend.fontsize'] = fs;

matplotlib.rcParams['font.family'] = ['DejaVu Sans']
matplotlib.rcParams['font.sans-serif'] = ['Arial']
matplotlib.rcParams['text.usetex'] = False

matplotlib.rcParams['svg.fonttype'] = 'none'

ordering = [0, 1, 2, 4, 3, 5, 6]
names = ["MPI_AllReduce", "MPI IAllReduce", "SSAR_Recursive_double", "SSAR_Split_allgather", "DSAR_Split_allgather", 'SSAR_Ring', "Dense AllReduce Ring"]
#names = ["MPI_AllReduce", "MPI IAllReduce", "SSAR_Rec_Dbl", "SSAR_Split_AlGa", "DSAR_Split_AlGa", 'SSAR_Ring', "Dense AllReduce Ring"]
style = ['k-o', 'k--o', 'b-o', 'm-o', 'r-o', 'g-o', 'g--o']

ignore = [1];
average = [0, 1, 6];
logLogScale = True

def process_folder(folder, pType, val1, val2, plotLegend, title, ign):

    ignore.append(ign);
    res = dict();

    for i in range(0, len(names)):
        res[i] = dict();

    if(pType == 1):
        file_re = re.compile('output_(?P<xval>[0-9]+)_{0}_\[{1}\].log'.format(val1, val2));
        #title = "Varying the dimension - Number of processes: '{0}' / density: '{1}'%".format(val1, float(val2)*100);
        xlabel = "Dimension of data";
    elif(pType == 2):
        file_re = re.compile('output_{0}_(?P<xval>[0-9]+)_\[{1}\].log'.format(val1, val2));
        #title = "Varying # of processes - Dimension: '{0}' / k: '{1}' ('{2}%)".format(val1, round(int(val1)*float(val2)), float(val2)*100);
        xlabel = "Number of nodes";
    else:
        file_re = re.compile('output_{0}_{1}_\[(?P<xval>[0-9.]+)\].log'.format(val1, val2));
        #title = "Varying the density of the data - Dimension: '{0}' / Number of nodes: '{1}'".format(val1, val2);
        dim = int(val1)
        xlabel = "Density of non-zero indices in percentage";
        #xlabe = "Number of non-zero indices";

    print(title);

    line_re = re.compile('\[(?P<v1>[0-9]+)\]: (?P<v2>[0-9.]+) secs');

    anyFile = False;
    for filename in os.listdir(folder):
        m_file_re = file_re.match(filename);
        if m_file_re is not None:
            with open(folder+'/'+filename, 'r') as f:
                anyFile = True;
                if pType == 3:
                    #xval = round(dim * float(m_file_re.group('xval')));
                    xval = float(m_file_re.group('xval'))*100;
                else:
                    #xval = sqrt(int(m_file_re.group('xval')));
                    xval = int(m_file_re.group('xval'));

                print("Processing: " + filename);

                for i in range(0, len(names)):
                    if xval not in res[i]:
                        res[i][xval] = [];

                for line in f:
                    m_line_re = line_re.search(line);
                    if m_line_re is not None:
                        idx = ordering[int(m_line_re.group('v1'))]
                        val = float(m_line_re.group('v2'));
                        res[idx][xval].append(val);

    if not anyFile:
        print("No output file found for those settings in folder: " + folder);
        exit();

    fig = plt.figure()
    ax = fig.add_axes((.1,.4,.8,.5))

    maxX = 0;
    minX = sys.maxsize;
    cnt = 0;
    for i,var in res.items():
        if i in ignore:
            continue
        od = OrderedDict(sorted(var.items(), key=lambda t: t[0]))
        x = od.keys();
        maxX = max(max(x), maxX);
        minX = min(min(x), minX);
        vals = od.values();

        if all(len(x) == 0 for x in vals):
            continue;

        if any(len(x) == 0 for x in vals):
            print("Some missing values for type '{0}'!".format(names[i]))
            continue;

        cnt = cnt + 1;

        if pType > 2 and i in average:
            allVals = [a for b in vals for a in b];
            y = [np.median(allVals)] * len(x);
            #minVals = [0] * len(x);
            #maxVals = [0] * len(x);
            minVals = [np.median(allVals) - np.percentile(allVals, 25)] * len(x);
            maxVals = [np.percentile(allVals, 75) - np.median(allVals)] * len(x);
        else:
            y = list(map(lambda v: np.median(v),vals));
            minVals = list(map(lambda v: np.median(v) - np.percentile(v, 25), vals));
            maxVals = list(map(lambda v: np.percentile(v, 75) - np.median(v), vals));
            #minVals = list(map(lambda v: np.min(v), vals));
            #maxVals = list(map(lambda v: np.max(v), vals));

        (_, caps, _) = plt.errorbar(x, y, yerr=[minVals, maxVals], label=names[i], fmt=style[i], linewidth=2, markersize=8, capsize=10);
        for cap in caps:
                cap.set_markeredgewidth(1)

        #minVals = map(lambda v: np.median(v) - min(v), vals);
        #maxVals = map(lambda v: max(v) - np.median(v), vals);
        #plt.plot(x, y, '-', color=colors[i], label=names[i]);
        #plt.fill_between(x, minVals, maxVals, color=colors[i], alpha=0.1);

    #ax.tick_params(axis='both');
    plt.grid(True);
    ax.set_axisbelow(True)
    ax.grid(color='lightgray', linestyle='-', linewidth=0.5)
    #ax.tick_params(direction='out', pad=5)
    #ax.yaxis.grid();
    if logLogScale:
        ax.set_xscale("log", nonposx='clip', basex=2)
        ax.set_yscale("log", nonposy='clip')
    plt.xlim([minX*0.8, maxX*1.2]);
    ax.set_xticks(list(x))
    #for axis in [ax.xaxis]:
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.3f'));
    ax.xaxis.set_major_formatter(ScalarFormatter());
    #ax.xaxis.set_major_formatter(FormatStrFormatter('%.3f'));
    #plt.ylim([-0.005, 0.15]);
    plt.xlabel(xlabel);
    plt.ylabel('Reduction time (s)');
    if(plotLegend):
        #columns = 3 if cnt < 4 else 2;
        #plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.2), ncol=columns);
        #plt.legend(loc="upper center", bbox_to_anchor=(0.5, -0.3), ncol=columns, prop={'size': 18});
        plt.legend(loc="center left", bbox_to_anchor=(1.15, 0.5));
    plt.title(title);
    print(title);

    #plt.tight_layout(0.4)
    #fig1 = plt.gcf()
    #mng = plt.get_current_fig_manager()
    #mng.resize(*mng.window.maxsize())
    #plt.draw();
    #plt.show();

    #fig.savefig("output.png");
    plt.savefig("output.png", bbox_inches='tight')
    #plt.close()

if __name__ == '__main__':
    if(len(sys.argv) < 7):
        print("You have to specify the directory, the x-axis value (1 = N / 2 = P / 3 = D) and fix the two other values (in that order)!");
    elif(len(sys.argv) == 7):
        process_folder(sys.argv[1], int(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5] == "True", sys.argv[6], 1);
    else:
        process_folder(sys.argv[1], int(sys.argv[2]), sys.argv[3], sys.argv[4], sys.argv[5] == "True", sys.argv[6], int(sys.argv[7]));
